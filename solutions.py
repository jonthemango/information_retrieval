"""
Write your reusable code here.
Main method stubs corresponding to each block is initialized here. Do not modify the signature of the functions already
created for you. But if necessary you can implement any number of additional functions that you might think useful to you
within this script.

Delete "Delete this block first" code stub after writing solutions to each function.

Write you code within the "WRITE YOUR CODE HERE vvvvvvvvvvvvvvvv" code stub. Variable created within this stub are just
for example to show what is expected to be returned. You CAN modify them according to your preference.
"""

import nltk
from nltk.stem.porter import *
import os
from bs4 import BeautifulSoup
import json

def block_reader(path):
    """ This function yields the raw text of individual .sgm files within a directory path
    """
    for fn in os.listdir(path):
        if fn.endswith('.sgm'):
            with open(os.path.join(path, fn), 'r') as f:
                contents = f.read()
                yield str(contents)
        


def block_document_segmenter(raw_sgm_content_generator):
    """ This function accepts an iterable of the content of raw sgm files, 
    and seperates the content into seperate documents, keeping the xml within them. 
    """
    for raw_sgm_content in raw_sgm_content_iterable:
        raw_sgm_content = raw_sgm_content.replace('<!DOCTYPE lewis SYSTEM "lewis.dtd">', "")
        wrapped_with_tag = '<X>' + raw_sgm_content + '</X>'
        soup = BeautifulSoup(wrapped_with_tag, 'xml')
        documents = soup.find_all('REUTERS')
        for document in documents:
            yield str(document)
        


def block_extractor(document_generator):
    """This function gets the raw text from beautiful soup of a document 
    and yields a series of dictionaries containing an id and text fields.
    """
    for document in document_generator:
        soup = BeautifulSoup(document, 'xml')
        dictionary = {'ID': soup.REUTERS["NEWID"], 'TEXT': soup.get_text()}
        yield dictionary


def block_tokenizer(document_generator):
    """This function takes the raw text from a series of documents and uses nltk
    to tokenize the words. The function yields back the id and the token.
    """
    for document in document_generator:
        for token in nltk.word_tokenize(document["TEXT"]):
            yield document["ID"], token



def block_stemmer(token_generator):
    """This function uses the PorterStemmer from nltk and stems tokens using that algoritm, 
    this function yields back stemmed tokens.
    """
    stemmer = PorterStemmer()
    for doc_id, token in token_generator:
        yield doc_id, stemmer.stem(token)


def block_stopwords_removal(token_generator, stopwords):
    """This function removes stopwords from a series of tokens, if the token is contained within stopwords,
    it is removed, all other tokens are yielded back.
    """
    stopwords = stopwords.split("\n")
    for doc_id, token in token_generator:
        if token not in stopwords:
            yield doc_id, token
